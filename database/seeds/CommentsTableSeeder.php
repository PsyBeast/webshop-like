<?php

use Illuminate\Database\Seeder;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $limit = 10;

        for ($i = 1; $i <= $limit; $i++) {
            for ($j = 1; $j <= 8; $j++) {
                DB::table('comments')->insert(
                    [
                        'user_id' => $i,
                        'product_id' => $j,
                        'body' => $faker->realText(200),
                        'created_at' => $faker->dateTimeThisDecade($max = 'now'),
                    ]
                );
            }
        }
    }
}
