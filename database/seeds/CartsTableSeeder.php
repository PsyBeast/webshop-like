<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CartsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('carts')->insert([
            [
                'quantity' => 2,
                'user_id' =>10,
                'product_id' => 7,
            ],
            [
                'quantity' => 1,
                'user_id' =>11,
                'product_id' => 6,
            ],
            [
                'quantity' => 3,
                'user_id' =>11,
                'product_id' => 5,
            ],

        ]);
    }
}
