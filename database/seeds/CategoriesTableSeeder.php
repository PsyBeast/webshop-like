<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            [
                'category_name' => 'T-Shirts',
                'picture_url' => '/images/categories/category-shirt.png',
            ],
            [
                'category_name' => 'Toys',
                'picture_url' => '/images/categories/category-toy.png',
            ]
        ]);
    }
}
