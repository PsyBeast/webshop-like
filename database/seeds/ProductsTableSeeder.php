<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            [
                'product_name' => 'Eat Sleep Code Repeat',
                'short_description' => 'Casual wear for programmers',
                'long_description' => 'You all know how it is when you forget to eat and sleep just because you see only
                the problem at hand',
                'picture_url' => '/images/products/shirt1.png',
                'category_id' => 1,
                'price' => 6000
            ],
            [
                'product_name' => 'Keep calm, keep coding',
                'short_description' => 'The (in)famous keep calm',
                'long_description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ut mauris tellus.
                Duis semper metus placerat diam scelerisque, et feugiat elit lobortis. Mauris auctor, sem eget dictum
                ornare, purus libero euismod libero, ac auctor lectus magna eget felis. Phasellus auctor erat et feugiat
                 commodo. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
                 Vestibulum pellentesque pellentesque ex, a commodo mauris pulvinar in. Cras ac maximus elit. Curabitur
                 dignissim ante nibh, ac ultrices tortor semper sit amet. Quisque a arcu quis nunc condimentum rhoncus
                 et id lacus.',
                'picture_url' => '/images/products/shirt2.png',
                'category_id' => 1,
                'price' => 2500
            ],
            [
                'product_name' => 'Can\'t see me rollin',
                'short_description' => 'Now you see me, now you don\'t',
                'long_description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ut mauris tellus.
                Duis semper metus placerat diam scelerisque, et feugiat elit lobortis. Mauris auctor, sem eget dictum
                ornare, purus libero euismod libero, ac auctor lectus magna eget felis. Phasellus auctor erat et feugiat
                 commodo. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
                 Vestibulum pellentesque pellentesque ex, a commodo mauris pulvinar in. Cras ac maximus elit. Curabitur
                 dignissim ante nibh, ac ultrices tortor semper sit amet. Quisque a arcu quis nunc condimentum rhoncus
                 et id lacus.',
                'picture_url' => '/images/products/shirt3.jpg',
                'category_id' => 1,
                'price' => 4500
            ],
            [
                'product_name' => 'Coffee and code',
                'short_description' => 'Its like fuel for a programmer',
                'long_description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ut mauris tellus.
                Duis semper metus placerat diam scelerisque, et feugiat elit lobortis. Mauris auctor, sem eget dictum
                ornare, purus libero euismod libero, ac auctor lectus magna eget felis. Phasellus auctor erat et feugiat
                 commodo. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
                 Vestibulum pellentesque pellentesque ex, a commodo mauris pulvinar in. Cras ac maximus elit. Curabitur
                 dignissim ante nibh, ac ultrices tortor semper sit amet. Quisque a arcu quis nunc condimentum rhoncus
                 et id lacus.',
                'picture_url' => '/images/products/shirt4.jpg',
                'category_id' => 1,
                'price' => 3500
            ],
            [
                'product_name' => 'RC car t-300',
                'short_description' => 'Press the button, it\'ll say WROOM WROOM',
                'long_description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ut mauris tellus.
                Duis semper metus placerat diam scelerisque, et feugiat elit lobortis. Mauris auctor, sem eget dictum
                ornare, purus libero euismod libero, ac auctor lectus magna eget felis. Phasellus auctor erat et feugiat
                 commodo. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
                 Vestibulum pellentesque pellentesque ex, a commodo mauris pulvinar in. Cras ac maximus elit. Curabitur
                 dignissim ante nibh, ac ultrices tortor semper sit amet. Quisque a arcu quis nunc condimentum rhoncus
                 et id lacus.',
                'picture_url' => '/images/products/toy2.png',
                'category_id' => 2,
                'price' => 15500
            ],
            [
                'product_name' => 'New Age LEGO',
                'short_description' => 'All hail the guy with the flaming staff',
                'long_description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ut mauris tellus.
                Duis semper metus placerat diam scelerisque, et feugiat elit lobortis. Mauris auctor, sem eget dictum
                ornare, purus libero euismod libero, ac auctor lectus magna eget felis. Phasellus auctor erat et feugiat
                 commodo. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
                 Vestibulum pellentesque pellentesque ex, a commodo mauris pulvinar in. Cras ac maximus elit. Curabitur
                 dignissim ante nibh, ac ultrices tortor semper sit amet. Quisque a arcu quis nunc condimentum rhoncus
                 et id lacus.',
                'picture_url' => '/images/products/toy3.jpg',
                'category_id' => 2,
                'price' => 2500
            ],
            [
                'product_name' => 'Green BIONICLE',
                'short_description' => 'If you kiss it it may turn to a prince ... or not',
                'long_description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ut mauris tellus.
                Duis semper metus placerat diam scelerisque, et feugiat elit lobortis. Mauris auctor, sem eget dictum
                ornare, purus libero euismod libero, ac auctor lectus magna eget felis. Phasellus auctor erat et feugiat
                 commodo. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
                 Vestibulum pellentesque pellentesque ex, a commodo mauris pulvinar in. Cras ac maximus elit. Curabitur
                 dignissim ante nibh, ac ultrices tortor semper sit amet. Quisque a arcu quis nunc condimentum rhoncus
                 et id lacus.',
                'picture_url' => '/images/products/toy4.jpg',
                'category_id' => 2,
                'price' => 2000
            ],
            [
                'product_name' => 'Spawn action figure',
                'short_description' => 'Wow this guy looks angry. Really scary I tell ya',
                'long_description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ut mauris tellus.
                Duis semper metus placerat diam scelerisque, et feugiat elit lobortis. Mauris auctor, sem eget dictum
                ornare, purus libero euismod libero, ac auctor lectus magna eget felis. Phasellus auctor erat et feugiat
                 commodo. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
                 Vestibulum pellentesque pellentesque ex, a commodo mauris pulvinar in. Cras ac maximus elit. Curabitur
                 dignissim ante nibh, ac ultrices tortor semper sit amet. Quisque a arcu quis nunc condimentum rhoncus
                 et id lacus.',
                'picture_url' => '/images/products/toy5.jpg',
                'category_id' => 2,
                'price' => 25500
            ],
        ]);
    }
}
