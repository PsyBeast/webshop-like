@extends('layouts.app')

@section('content')
    <div class="container product">
        <div class="row">
            <ul class="cart-items col-md-10 col-md-offset-1">

                @foreach($user->carts as $cart)
                    <li>
                        @foreach($cart->products as $product)
                            <div class="row itemrow media">
                                <div class="media-left media-middle col-md-2">
                                    <img src="{{$product->picture_url}}"
                                         alt="{{  $product->product_name }}" class="media-object">
                                </div>
                                <div class="col-md-10 item-description">
                                    <p class="row">
                                        <span class="item-name col-md-7">{{ $product->product_name }}</span>
                                        <span class="item-quantity col-md-2">Qty: {{ $cart->quantity }}</span>
                                        <span class="item-price text-right col-md-2">{{ $product->price }} Ft</span>
                                        <span class="item-remove col-md-1">
                                            <a href="#" class="btn btn-danger">
                                                <i class="glyphicon glyphicon-remove"></i>
                                            </a>
                                        </span>
                                    </p>
                                </div>
                            </div>
                        @endforeach
                    </li>
                @endforeach

            </ul>
        </div>
    </div>
@stop