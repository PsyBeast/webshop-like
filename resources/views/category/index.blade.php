@extends('layouts.app')

@section('content')
    <div class="container">
        <h1 class="text-center">Categories</h1>

        <ul class="row categories">

            @foreach ($categories as $category)

                <li class="text-center col-sm-6 col-md-3 col-lg-2">
                    <a href="/categories/{{ $category->id }}/products">
                        <div class="col-md-12 card">
                            <h2 class="card-title"> {{ $category->category_name }}</h2>
                            <img src="{{ $category->picture_url }}" class="img-responsive">
                        </div>
                    </a>
                </li>

            @endforeach

            @foreach ($categories as $category)

                <li class="text-center col-sm-6 col-md-3 col-lg-2">
                    <a href="/categories/{{ $category->id }}/products">
                        <div class="col-md-12 card">
                            <h2 class="card-title"> {{ $category->category_name }}</h2>
                            <img src="{{ $category->picture_url }}" class="img-responsive">
                        </div>
                    </a>
                </li>

            @endforeach

            @foreach ($categories as $category)

                <li class="text-center col-sm-4 col-md-3 col-lg-2">
                    <a href="/categories/{{ $category->id }}/products">
                        <div class="col-md-12 card">
                            <h2 class="card-title"> {{ $category->category_name }}</h2>
                            <img src="{{ $category->picture_url }}" class="img-responsive">
                        </div>
                    </a>
                </li>

            @endforeach



        </ul>

    </div>

@stop