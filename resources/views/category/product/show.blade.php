@extends('layouts.app')

@section('content')
    <div class="container product">

        <!-- Product details ------------------------------------- -->

        <div class="row product-details">
            <div class="media col-md-10 col-md-offset-1">
                <div class="media-left media-middle">
                    <img src="{{ $product->picture_url }}" alt="{{ $product->product_name }}"
                         class="media-object">
                </div>
                <div class="media-body">
                    <h1 class="productname">{{ $product->product_name }}</h1>
                    <p class="short_desc">{{ $product->short_description }}</p>
                    <p class="long_desc">{{ $product->long_description }}</p>

                    <div class="pricerow">
                        <p>Price: <span class="price">{{ $product->price }} Ft</span></p>
                    </div>
                    <form class="form-inline" method="POST" action="/cart/products/{{ $product->id }}">

                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="quantity">Quantity</label>
                            <input type="number" name="quantity" class="form-control" id="quantity" placeholder="0"
                                   min="0">
                            <button type="submit" class="btn btn-primary">
                                <i class="glyphicon glyphicon-shopping-cart"></i>Add to cart
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- Comments ------------------------------------- -->

        <div class="product-comments row">
            <div class="col-md-10 col-md-offset-1">
                <h1>Comments</h1>

                <!-- Add Comment ------------------------------------- -->

                <div class="row">

                    <a class="btn btn-primary add-comment"><i class="glyphicon glyphicon-plus-sign"></i>Add Comment</a>

                    <form method="POST" action="{{ $product->id }}/comments" class="post-comment form-group">

                        {{ csrf_field() }}

                        <textarea name="body" class="form-control" placeholder="Type your message here"></textarea>

                        <input type="submit" class="btn btn-primary send-comment" value="Send">

                        <a class="btn btn-default cancel-comment">Cancel</a>

                        <p class="alert alert-success result"></p>

                    </form>

                    @if (count($errors))

                        <ul class="alert alert-danger col-md-12">
                            @foreach ($errors->all() as $error)

                                <li><i class="glyphicon glyphicon-exclamation-sign"></i>{{ $error }}</li>

                            @endforeach
                        </ul>

                    @endif

                </div>

                <!-- List Comments ------------------------------------- -->

                <ul class="row">
                    @foreach($product->comments as $comment)
                        <li class="panel panel-default">
                            <p class="panel-heading author"><a href="#"> {{ $comment->user->name }}</a>
                                <span class="pull-right">{{ $comment->created_at }} </span>
                            </p>
                            <p class="panel-body">{{$comment->body}}</p>

                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script>
        $(document).ready(function () {

            //Show add comment section
            $('.add-comment').on('click', function() {
                showAddComment($(this));
            });

            //Hide add comment section
            $('.cancel-comment').on('click', function(){
                hideAddComment();
            });

            $('.send-comment').on('click', function(e){
                //e.preventDefault();

                //$.post('{{ $product->id }}/comments', $('.post-comment').serialize());
            });

            function showAddComment(elem){
                $(elem).toggleClass('disabled');
                $('.post-comment').show(300);
            }

            function hideAddComment(){
                $('.post-comment textarea').val('');
                $('.post-comment').hide(300);
                $('.add-comment').toggleClass('disabled');
            }
        });
    </script>

@stop