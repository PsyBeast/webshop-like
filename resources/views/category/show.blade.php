@extends('layouts.app')

@section('content')
    <div class="container">
        <h1 class="text-center">{{ $category->category_name }}</h1>

        <ul class="row products">

            @foreach ($category->products as $product)

                <li class="text-center col-sm-6 col-md-3 col-lg-3">

                    <div class="col-md-12 card">
                        <a href="/categories/{{ $category->id }}/products/{{ $product->id }}">

                            <h2 class="card-title">{{ $product->product_name }}</h2>

                            <img src="{{ $product->picture_url }}" class="col-md-12 img-responsive">

                            <p>{{ $product->short_description }}</p>

                            <span class="price">{{ $product->price }} Ft</span>

                        </a>
                    </div>


                </li>

            @endforeach

            @foreach ($category->products as $product)

                <li class="text-center col-sm-6 col-md-3 col-lg-3">

                    <div class="col-md-12 card">
                        <a href="/categories/{{ $category->id }}/products/{{ $product->id }}">

                            <h2 class="card-title">{{ $product->product_name }}</h2>

                            <img src="{{ $product->picture_url }}" class="col-md-12 img-responsive">

                            <p>{{ $product->short_description }}</p>

                            <span class="price">{{ $product->price }} Ft</span>

                        </a>
                    </div>


                </li>

            @endforeach


        </ul>

    </div>

@stop