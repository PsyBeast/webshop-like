<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['body'];

    public function product()
    {
        $this->belongsTo(Product::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }


/*
    public function addComment(Comment $comment, $userId)
    {
        $comment->user_id = $userId;

        return $this->comments()->save($comment);
    }*/
}
