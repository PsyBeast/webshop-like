<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Comment;

class Product extends Model
{
    public function category()
    {
        $this->belongsTo(Category::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }



    public function addComment(Comment $comment, User $user)
    {
        $comment->user_id = $user->id;

        return $this->comments()->save($comment);
    }
}
