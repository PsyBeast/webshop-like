<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Cart;


class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'created_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function carts()
    {
        return $this->hasMany(Cart::class);
    }

    public function roles()
    {
        return $this->belongsToMany('Role', 'users_roles');
    }

    public function addCart(Cart $cart)
    {
        return $this->carts()->save($cart);
    }

    public function isAdmin()
    {
        return in_array('admin', array_fetch($this->roles->toArray(), 'role_name'));
    }
}
