<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use App\Product

class Cart extends Model
{
    protected $fillable = ['product_id', 'quantity'];

    public function products()
    {
        return $this->hasMany('App\Product', 'id', 'product_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'id');
    }
}
