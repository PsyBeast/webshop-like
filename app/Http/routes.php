<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::group(['middlewareGroups' => ['web']], function() {
    //authentication
    Route::auth();

    //show all categories
    Route::get('/categories', 'CategoriesController@index');

    //show a category's products
    Route::get('/categories/{category}/products', 'CategoriesController@show');

    //show specific product
    Route::get('/categories/{category}/products/{product}', 'ProductsController@show');

    //Add comment
    Route::post('/categories/{category}/products/{product}/comments', 'CommentsController@store');

    //Show cart
    Route::get('/cart', 'CartsController@show');

    //Add to cart
    Route::post('/cart/products/{product}', 'CartsController@store');


});
