<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

use App\Category;
use App\Product;


class ProductsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category, Product $product)
    {
        $product->load(['comments' => function ($query) {
            $query->select('comments.*')->join('users', 'comments.user_id', '=', 'users.id')->orderBy('comments.created_at', 'desc');
        }
        ]);

        return view('category.product.show', compact('product'));
    }
}
